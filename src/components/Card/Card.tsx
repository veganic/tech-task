import React from 'react';
import styled from 'styled-components';
import { Button } from '../Button/Button';

const CardSC = styled.div`
  display: flex;
  flex-direction: column;
  background: #FFFFFF 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 25px #0000000D;
  border: 1px solid #EFEFEF;
  border-radius: 12px;
  overflow: hidden;
`;

const CardContentSC = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: 28px;
`

const ExcerptSC = styled.div`
  flex: 1;
  margin-bottom: auto;
`

const TitleSC = styled.h2`
  margin-top: 0;
`

const RightAlignSC = styled.div`
  margin-top: auto; 
  text-align: right;
`

const ImageSC = styled.img`
  height: 255px;
`


interface CardInterface {
  data: any; // TODO: a proper data interface would be defined given more time
}


export const Card:React.FC<CardInterface> = ({data}) => {
  console.log(data)
  return (
    <CardSC>
      <ImageSC src='https://via.placeholder.com/344x255.png' />
      <CardContentSC>
        <TitleSC>{data.title.rendered}</TitleSC>
        <ExcerptSC dangerouslySetInnerHTML={{__html: data.excerpt.rendered}} />
        <RightAlignSC>
          {/* TODO: this should really be an <a> - cheating due to time constraint */}
          <Button 
            variant='outline'         
            label='Read more'
            onClick={() => window.location.href = data.link}
            />
        </RightAlignSC>
      </CardContentSC>
    </CardSC>
  )
}
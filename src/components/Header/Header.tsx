
import React from 'react';
import { useState } from 'react';
import styled from 'styled-components';
import { contentWidth } from '../../constants/style';
import { Button } from '../Button/Button';

const HeaderSC = styled.div`
  position: relative;
  height: 302px;
  background: transparent linear-gradient(97deg, #511999 0%, #431679 50%, #8547AD 100%) 0% 0% no-repeat padding-box;
`

const FetchBoxSC = styled.div`
  position: absolute;
  top: 196px;
  left: 50%;
  transform: translate(-50%, 0);
  margin: 0 auto;
  width: ${contentWidth};
  background: #FFFFFF 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 25px #0000000D;
  border: 1px solid #EFEFEF;
  border-radius: 14px;
  padding: 27px 56px;
  box-sizing: border-box;
`

const InputGroupSC = styled.div`
  display: flex;
`

const InputLabelSC = styled.label`
  display: block;
  font-size: 18px;
  font-weight: bold;
  color: #684394;
  padding-bottom: 12px;
`

const InputSC = styled.input`
  flex: 1;
  font-size: 18px;
  background: #F7F7F7 0% 0% no-repeat padding-box;
  border: 1px solid #EDEDED;
  border-radius: 9px;
  padding: 10px 20px;
  margin-right: 54px;
`


interface HeaderInterface {
  onLoad: (results: any[]) => void;
}

export const Header:React.FC<HeaderInterface> = ({onLoad}) => {
  const [url, setUrl] = useState('https://www.bewley.co.uk/wp-json/wp/v2/posts');
  const [loading, setLoading] = useState(false);

  const fetchURL = () => {
    setLoading(true);

    fetch(url)
    .then((result) => result.json())
    .then((result) => onLoad(result))
    .catch(e => alert(`Unable to fetch ${url} (Note: URL validation and better error handling would be implemented in a deployed app)`))
    .finally(() => setLoading(false))
  }

  return (
    <HeaderSC>
      <FetchBoxSC>
        <InputLabelSC>Enter URL</InputLabelSC>
        <InputGroupSC>
          <InputSC 
            value={url} 
            onChange={(e) => setUrl(e.target.value)}/>
          <Button 
            label='Fetch' 
            isLoading={loading}
            onClick={fetchURL} />
        </InputGroupSC>
      </FetchBoxSC>
    </HeaderSC>
  )
}
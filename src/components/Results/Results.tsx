import React, { useState } from 'react';
import styled from 'styled-components';
import { contentWidth } from '../../constants/style';
import { Card } from '../Card/Card';

const GridSC = styled.div`
  max-width: ${contentWidth};
  margin: 88px auto;
  
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(344px, 1fr));
  column-gap: 35px;
  row-gap: 27px;
`;

interface ResultsInterface {
  data: any[];
}


export const Results:React.FC<ResultsInterface> = ({data}) => { 
  const [amount, setAmount] = useState(6);

  return (
    <GridSC>
      {
        data.slice(0, amount).map((item, i) => <Card key={i} data={item} />)
      }

      {/* TODO: add Load More button which increases the amount */}
    </GridSC>
  )
}
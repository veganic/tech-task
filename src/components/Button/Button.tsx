
import React from 'react';
import styled from 'styled-components';

const ButtonSC = styled.button<{variant?: string}>`
  border-radius: 23px;
  background: ${p => p.variant === 'outline' ? 'white' : '#8547AD linear-gradient(102deg, #511999 0%, #431679 50%, #8547AD 100%) 0% 0%'};
  box-shadow: ${p => p.variant === 'outline' ? 'none' : '0px 3px 8px #00000038'};
  color: ${p => p.variant === 'outline' ? '#684394' : '#FFFFFF'};
  border: ${p => p.variant === 'outline' ? '1px solid #E6E6E6' : '0'};
  font-size: 16px;
  font-weight: bold;
  padding: 12px 26px;
  transition: all 0.5s;

  :hover {
    background: #511999;
    color: #fff;
  }
`

// loading spinner animation would go here
const SpinnerSC = styled.span<{isLoading?: boolean}>`
  margin-left: 100px;
  opacity: ${p => p.isLoading ? 0.5 : 0};
`

// external link icon would go here
const ExternalLinkSC = styled.span`
  margin-left: 25px;

`

interface ButtonInterface extends React.HTMLProps<HTMLButtonElement> {
  variant?: string;
  isLoading?: boolean;
}


export const Button:React.FC<ButtonInterface> = ({label, variant, isLoading, onClick}) => {
  return (
    <ButtonSC variant={variant} onClick={onClick}>
      {label}
      {variant === 'outline' 
      ? <ExternalLinkSC />
      : <SpinnerSC isLoading={isLoading}>&#8635;</SpinnerSC>
      }
    </ButtonSC>
  )
}
import './App.css';
import Montserrat from './assets/fonts/Montserrat/Montserrat-Regular.ttf'
import { Header } from './components/Header/Header'
import { Results } from './components/Results/Results'

import styled, { createGlobalStyle } from 'styled-components';
import { useState } from 'react';


const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Montserrat';
    src: url(${Montserrat}) format('truetype');
    font-weight: 300;
    font-style: normal;
    font-display: auto;
  } 

  *{
    font-family: 'Montserrat';
  }

  h2 {
    font-size: 16px;
    font-weight: bold;
    color: #684394;
  }

  p {
    font-size: 14px;
    color: #4D4D4D;
  }
`;

const AppSC = styled.div`
  background: #FCFCFC;
`


function App() {
  const [data, setData] = useState([]);

  return (
    <>
      <GlobalStyle />
      <AppSC>
        <Header onLoad={setData} />
        <Results data={data} />
      </AppSC>
    </>
  );
}

export default App;
